#!/bin/ash
set -ex

REPO_LIST=
collect() {
  RESPONSE=$(curl -s "$1")
  NEXT=$(echo "${RESPONSE}" | jq -r '.next')
  REPO_LIST="${REPO_LIST} $(echo "${RESPONSE}" | jq -r '.results|.[]|.name')"
  if [ "${NEXT}" != 'null' ]; then
    collect "${NEXT}"
  fi
}
collect "https://hub.docker.com/v2/repositories/${OWNER}/?page_size=100"

BUILDX_VER=$(curl -ks https://api.github.com/repos/docker/buildx/releases/latest | jq -r '.name')
mkdir -p "$HOME/.docker/cli-plugins/"
wget -O "$HOME/.docker/cli-plugins/docker-buildx" "https://github.com/docker/buildx/releases/download/${BUILDX_VER}/buildx-${BUILDX_VER}.linux-amd64"
chmod a+x "$HOME/.docker/cli-plugins/docker-buildx"
echo -e '{\n  "experimental": "enabled"\n}' | tee "$HOME/.docker/config.json"
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --use --name builder
docker buildx inspect --bootstrap builder
docker buildx install

for REPO in ${REPO_LIST}; do
  docker buildx build --cache-to=type=local,dest=cache,mode=max --platform linux/amd64 --build-arg IMAGE="${OWNER}/${REPO}" -t "${OWNER}/${REPO}:amd64" . || true
  docker buildx build --cache-to=type=local,dest=cache,mode=max --platform linux/arm64 --build-arg IMAGE="${OWNER}/${REPO}" -t "${OWNER}/${REPO}:arm64" . || true
  docker buildx build --cache-to=type=local,dest=cache,mode=max --platform linux/arm/v7 --build-arg IMAGE="${OWNER}/${REPO}" -t "${OWNER}/${REPO}:armv7" . || true
done
